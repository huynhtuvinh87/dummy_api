<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'parent_id', 'user_id', 'level', 'title', 'slug', 'description'
    ];


    public function children()
    {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id')->select('id','parent_id', 'title', 'slug', 'description');
    }

    public function parent()
    {
        return $this->hasOne('App\Models\Category', 'id', 'parent_id')->select('id', 'parent_id', 'title', 'slug', 'description');
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
}
