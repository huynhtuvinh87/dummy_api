<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Repositories\UserRepository;
use Illuminate\Http\Response;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository)
    {
        parent::__construct();
        $this->_userRepository = $userRepository;
    }

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        try {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $user = Auth::user();
                $success['token'] =  $user->createToken('MyApp')->accessToken;
                $success['name'] =  $user->name;
                return response()->json(['meta' => ['code' => Response::HTTP_OK, 'message' => 'OK'], 'result' => $success], Response::HTTP_OK);
            } else {
                return response()->json(['meta' => ['code' => 401, 'message' => ['error' => 'Unauthorised']]], 401);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required',
                'password_confirmation' => 'required|same:password',
            ]);
            if ($validator->fails()) {
                return response()->json(['meta' => ['code' => 401, 'error' => $validator->errors()]], 401);
            }
            $user = $this->_userRepository->register($request);
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['name'] =  $user->name;
            return response()->json(['meta' => ['code' => Response::HTTP_OK, 'message' => 'OK'], 'result' => $success], Response::HTTP_OK);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * logout api
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        try {
            $request->user()->token()->revoke();
            return response()->json(['meta' => ['code' => Response::HTTP_OK, 'message' => 'OK']], Response::HTTP_OK);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * me api
     *
     * @return \Illuminate\Http\Response
     */
    public function me()
    {
        try {
            $user = Auth::user();
            return response()->json(['meta' => ['code' => Response::HTTP_OK, 'message' => 'OK'], 'result' => $user], Response::HTTP_OK);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Update api
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:users,email,' . Auth::user()->id
            ]);
            if ($validator->fails()) {
                return response()->json(['meta' => ['code' => 401, 'error' => $validator->errors()]], 401);
            }
            $user = $this->_userRepository->update(Auth::user()->id, $request);
            return response()->json(['meta' => ['code' => Response::HTTP_OK, 'message' => 'OK'], 'result' => $this->_userRepository->getUser(Auth::user()->id)], Response::HTTP_OK);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Change password api
     *
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'password' => 'required|min:6',
                'password_confirmation' => 'required|same:password',
            ]);
            if ($validator->fails()) {
                return response()->json(['meta' => ['code' => 401, 'error' => $validator->errors()]], 401);
            }
            $user = $this->_userRepository->changePassword(Auth::user()->id, $request);
            return response()->json(['meta' => ['code' => Response::HTTP_OK, 'message' => 'OK'], 'result' => $this->_userRepository->getUser(Auth::user()->id)], Response::HTTP_OK);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function uploadBase64(Request $request)
    {

        try {
            $avatar = $this->_userRepository->uploadBase64($request->file);
            if (!$avatar) {
                return response()->json(['meta' => ['code' => Response::HTTP_NOT_MODIFIED, 'message' => 'ERROR']], Response::HTTP_NOT_MODIFIED);
            }
            return response()->json(['meta' => ['code' => Response::HTTP_OK, 'message' => 'OK'], 'result' => $avatar], Response::HTTP_OK);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function upload(Request $request)
    {

        try {
            $avatar = $this->_userRepository->upload($request->file);
            return response()->json(['meta' => ['code' => Response::HTTP_OK, 'message' => 'OK'], 'result' => $avatar], Response::HTTP_OK);
        } catch (Exception $e) {
            throw $e;
        }
    }
}
