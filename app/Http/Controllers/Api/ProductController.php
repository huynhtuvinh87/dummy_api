<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ProductRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Response;

class ProductController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ProductRepository $productRepository, UserRepository $userRepository)
    {
        parent::__construct();
        $this->_productRepository = $productRepository;
        $this->_userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            if ($this->checkAccessToken) {
                $query = $this->_productRepository->list($this->accessToken, $request->limit);
                return response()->json(['meta' => ['code' => Response::HTTP_OK, 'message' => 'OK'], 'result' => $query], Response::HTTP_OK);
            } else {
                return response()->json(['status' => '401', 'message'  => 'Error without access token.'], '401');
            }
        } catch (Exception $e) {
            throw $e;
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function category(Request $request)
    {
        try {
            if ($this->checkAccessToken) {
                $query = $this->_productRepository->productCategory($this->accessToken, $request->category_id, $request->limit);
                return response()->json(['meta' => ['code' => Response::HTTP_OK, 'message' => 'OK'], 'result' => $query], Response::HTTP_OK);
            } else {
                return response()->json(['status' => '401', 'message'  => 'Error without access token.'], '401');
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if ($this->checkAccessToken && $this->_userRepository->view($this->accessToken)) {
                $request->user_id = $this->_userRepository->view($this->accessToken)->id;
                $query = $this->_productRepository->create($request);
                return response()->json(['meta' => ['code' => Response::HTTP_OK, 'message' => 'OK'], 'result' => $query], Response::HTTP_OK);
            } else {
                return response()->json(['status' => '401', 'message'  => 'Error without access token.'], '401');
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if ($this->checkAccessToken && $this->_userRepository->view($this->accessToken)) {
                $query = $this->_productRepository->view($id, $this->accessToken);
                return response()->json(['meta' => ['code' => Response::HTTP_OK, 'message' => 'OK'], 'result' => $query], Response::HTTP_OK);
            } else {
                return response()->json(['status' => '401', 'message'  => 'Error without access token.'], '401');
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            if ($this->checkAccessToken && $this->_userRepository->view($this->accessToken)) {
                $query = $this->_productRepository->update($id, $request);
                return response()->json(['meta' => ['code' => Response::HTTP_OK, 'message' => 'OK'], 'result' => $query], Response::HTTP_OK);
            } else {
                return response()->json(['status' => '401', 'message'  => 'Error without access token.'], '401');
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if ($this->checkAccessToken && $this->_userRepository->view($this->accessToken)) {
                $result = $this->_productRepository->delete($id);
                return response()->json(['meta' => ['code' => Response::HTTP_OK, 'message' => 'OK'], 'result' => $result], Response::HTTP_OK);
            } else {
                return response()->json(['status' => '401', 'message'  => 'Error without access token.'], '401');
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
}
