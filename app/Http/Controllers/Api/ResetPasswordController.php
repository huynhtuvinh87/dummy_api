<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Support\Str;
use App\User;
use Illuminate\Http\Request;
use App\Models\PasswordReset;
use App\Notifications\ResetPasswordRequest;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;

class ResetPasswordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository, User $user, PasswordReset $passwordReset)
    {
        parent::__construct();
        $this->_userRepository = $userRepository;
        $this->_user = $user;
        $this->_passwordReset = $passwordReset;
    }

    /**
     * Create token password reset.
     *
     * @param  ResetPasswordRequest $request
     * @return JsonResponse
     */
    public function sendMail(Request $request)
    {
        try {
            $user = $this->_user->where('email', $request->email)->firstOrFail();
            $passwordReset = $this->_passwordReset->updateOrCreate([
                'email' => $user->email,
            ], [
                'token' => Str::random(60),
            ]);
            if ($passwordReset) {
                $user->notify(new ResetPasswordRequest($passwordReset->token));
            }
            return response()->json(['meta' => ['code' => Response::HTTP_OK, 'message' => 'OK'], 'result' => $user], Response::HTTP_OK);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function reset(Request $request, $token)
    {
        try {
            $passwordReset = $this->_passwordReset->where('token', $token)->firstOrFail();
            if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
                $passwordReset->delete();
                return response()->json(['meta' => ['code' => 422, 'message' => 'This password reset token is invalid.'], 'result' => $updatePasswordUser], 422);
            }
            $user = $this->_user->where('email', $passwordReset->email)->firstOrFail();
            $updatePasswordUser = $user->update($request->only('password'));
            return response()->json(['meta' => ['code' => Response::HTTP_OK, 'message' => 'OK'], 'result' => $updatePasswordUser], Response::HTTP_OK);
            $passwordReset->delete();
        } catch (Exception $e) {
            throw $e;
        }
    }
}
