<?php

namespace App\Repositories;

use App\Models\Category;
use App\User;
use App\Contracts\CategoryInterface;
use Exception;
use Illuminate\Support\Str;

class CategoryRepository implements CategoryInterface
{

    protected $_category;
    protected $_user;

    public function __construct()
    {
        $this->_category = new Category();
        $this->_user = new User();
    }
    /**
     * Get list Employee
     * @author huynhtuvinh87@gmail.com
     */
    public function list($access_token, $limit = 1)
    {
        try {
            $data = [];
            $category = $this->_category->select('categories.id', 'parent_id', 'title', 'slug', 'description')->leftjoin('users', function ($q) {
                $q->on('categories.user_id', 'users.id');
            })->where('users.access_token', $access_token)->where('categories.parent_id', null)->get();
            if ($category) {
                foreach ($category as $key => $value) {
                    $children2 = [];
                    if ($value->children) {
                        foreach ($value->children as $chil) {
                            $children2[] = $chil->children;
                        }
                    }
                    $data[] = [
                        'id' => $value->id,
                        'title' => $value->title,
                        'slug' => $value->slug,
                        'description' => $value->description,
                        'children' => $value->children
                    ];
                }
            }

            return $data;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getParent($parent_id, $data = null)
    {
        try {

            $category = $this->_category->select('id', 'parent_id', 'title', 'slug', 'description')->where('id', $parent_id)->first();
            if ($category) {
                $data[] = $category->toArray();
                if ($category->parent) {
                    $data[] = $category->parent->toArray();
                    if ($category->parent->parent) {
                        $data[] = $category->parent->parent->toArray();
                    }
                }
            }

            return $data;
        } catch (Exception $e) {
            throw $e;
        }
    }


    /**
     * Get view
     * @author huynhtuvinh87@gmail.com
     */
    public function view($id, $access_token)
    {
        try {
            $query = $this->_category->select(
                'categories.id',
                'parent_id',
                'slug',
                'title',
                'description'
            )->leftjoin('users', function ($q) {
                $q->on('categories.user_id', 'users.id');
            })->where('users.access_token', $access_token)->where('categories.id', $id)->first();
            return $query;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Create Employee
     *
     * @author huynhtuvinh87@gmail.com
     */
    public function create($request)
    {
        try {
            if ($this->level($request->parent_id) == 4) {
                return 'Sub category should not exceed 3 levels.';
            }
            $result = $this->_category->create([
                'user_id' => $request->user_id,
                'parent_id' => $request->parent_id,
                'level' => $this->level($request->parent_id),
                'title' => $request->title,
                'slug' => Str::slug($request->title, '-'),
                'description' => $request->description
            ]);
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }


    /**
     * Update projects
     * @param string name
     * @param string description
     * @param date start_time
     * @param date end_time
     *
     * @author huynhtuvinh87@gmail.com
     */
    public function update($id, $request)
    {
        try {
            if ($this->level($request->parent_id) == 4) {
                return 'Sub category should not exceed 3 levels.';
            }
            $result = $this->_category->where('id', $id)->update([
                'parent_id' => $request->parent_id,
                'level' => $this->level($request->parent_id),
                'title' => $request->title,
                'slug' => Str::slug($request->title, '-'),
                'description' => $request->description
            ]);
            return $this->_category->where('id', $id)->first();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Delete employees
     * @param string id
     *
     * @author huynhtuvinh87@gmail.com
     */
    public function delete($id)
    {
        try {
            $result = $this->_category->where('id', $id)->delete();
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Get view
     * @author huynhtuvinh87@gmail.com
     */
    public function level($id)
    {
        try {
            if ($id) {
                $query = $this->_category->select('level')->where('id', $id)->first();
                return $query->level + 1;
            } else {
                return 1;
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
}
