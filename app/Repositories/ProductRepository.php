<?php

namespace App\Repositories;

use App\Models\Product;
use App\User;
use App\Contracts\ProductInterface;
use Exception;
use Illuminate\Support\Str;

class ProductRepository implements ProductInterface
{

    protected $_product;
    protected $_user;

    public function __construct()
    {
        $this->_product = new Product();
        $this->_user = new User();
    }
    /**
     * Get list Employee
     * @author huynhtuvinh87@gmail.com
     */
    public function list($access_token, $limit)
    {
        try {

            $query = $this->_product->select(
                'products.id',
                'products.slug',
                'products.code',
                'products.title',
                'products.content',
                'products.image',
                'products.price',
                'products.price_sale',
                'products.quantily',
                'products.status',
                'categories.id as category_id',
                'categories.title as category_title',
                'categories.slug as category_slug'
            )->leftjoin('users', function ($q) {
                $q->on('products.user_id', 'users.id');
            })->leftjoin('categories', function ($q) {
                $q->on('products.category_id', 'categories.id');
            })->where('users.access_token', $access_token);
            return $query->orderBy('products.id', 'desc')->paginate($limit);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Get list Employee
     * @author huynhtuvinh87@gmail.com
     */
    public function productCategory($access_token, $category_id, $limit)
    {
        try {
            $query = $this->_product->select(
                'products.id',
                'products.slug',
                'products.code',
                'products.title',
                'products.content',
                'products.image',
                'products.price',
                'products.price_sale',
                'products.quantily',
                'products.status',
                'categories.id as category_id',
                'categories.parent_id as category_parent',
                'categories.title as category_title',
                'categories.slug as category_slug'
            )->leftjoin('users', function ($q) {
                $q->on('products.user_id', 'users.id');
            })->leftjoin('categories', function ($q) {
                $q->on('products.category_id', 'categories.id');
            })->where('users.access_token', $access_token)->where('products.category_id', $category_id);
            return $query->orderBy('products.id', 'desc')->paginate($limit);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Get edit Employee
     * @author huynhtuvinh87@gmail.com
     */
    public function view($id, $access_token)
    {
        try {
            $query = $this->_product->select(
                'products.id',
                'products.code',
                'products.title',
                'products.content',
                'products.image',
                'products.price',
                'products.price_sale',
                'products.quantily',
                'products.status'
            )->leftjoin('users', function ($q) {
                $q->on('products.user_id', 'users.id');
            })->where('users.access_token', $access_token)->where('id', $id)->first();
            return $query;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Create Employee
     *
     * @author huynhtuvinh87@gmail.com
     */
    public function create($request)
    {
        try {
            $result = $this->_product->create([
                'user_id' => $request->user_id,
                'category_id' => $request->category_id,
                'title' => $request->title,
                'slug' => Str::slug($request->title, '-'),
                'code' => str_pad($this->max(), 10, "0", STR_PAD_LEFT),
                'content' => $request->content,
                'image' => $request->image,
                'price' => $request->price,
                'price_sale' => $request->price_sale,
                'quantily' => $request->quantily,
                'status' => $request->status
            ]);
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }


    /**
     * Update projects
     * @param string name
     * @param string description
     * @param date start_time
     * @param date end_time
     *
     * @author huynhtuvinh87@gmail.com
     */
    public function update($id, $request)
    {
        try {
            $result = $this->_product->where('id', $id)->update([
                'category_id' => $request->category_id,
                'title' => $request->title,
                'slug' => Str::slug($request->title, '-'),
                'code' => str_pad($this->max(), 10, "0", STR_PAD_LEFT),
                'content' => $request->content,
                'image' => $request->image,
                'price' => $request->price,
                'price_sale' => $request->price_sale,
                'quantily' => $request->quantily,
                'status' => $request->status
            ]);
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Delete employees
     * @param string id
     *
     * @author huynhtuvinh87@gmail.com
     */
    public function delete($id)
    {
        try {
            $result = $this->_product->where('id', $id)->delete();
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function max()
    {
        try {
            $result = $this->_product->max('id');
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }
}
