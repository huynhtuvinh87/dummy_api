<?php

namespace App\Repositories;

use App\Models\Article;
use App\User;
use App\Contracts\ArticleInterface;
use Exception;

class ArticleRepository implements ArticleInterface
{

    protected $_article;
    protected $_user;

    public function __construct()
    {
        $this->_article = new Article();
        $this->_user = new User();
    }
    /**
     * Get list Employee
     * @author huynhtuvinh87@gmail.com
     */
    public function list($access_token, $limit = 1)
    {
        try {

            $query = $this->_article->select(
                'articles.id',
                'articles.title',
                'articles.description',
                'articles.content',
                'articles.image',
                'articles.status'
            )->leftjoin('users', function ($q) {
                $q->on('articles.user_id', 'users.id');
            })->where('users.access_token', $access_token);
            return $query->orderBy('articles.id', 'desc')->paginate($limit);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Get edit Employee
     * @author huynhtuvinh87@gmail.com
     */
    public function view($id, $access_token)
    {
        try {
            $query = $this->_article->select(
                'id',
                'title',
                'description',
                'content',
                'image',
                'status'
            )->leftjoin('users', function ($q) {
                $q->on('articles.user_id', 'users.id');
            })->where('users.access_token', $access_token)->where('id', $id)->first();
            return $query;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Create Employee
     *
     * @author huynhtuvinh87@gmail.com
     */
    public function create($request)
    {
        try {
            $result = $this->_article->create([
                'user_id' => $request->user_id,
                'title' => $request->title,
                'description' => $request->description,
                'content' => $request->content,
                'image' => $request->image,
                'status' => $request->status
            ]);
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }


    /**
     * Update projects
     * @param string name
     * @param string description
     * @param date start_time
     * @param date end_time
     *
     * @author huynhtuvinh87@gmail.com
     */
    public function update($id, $request)
    {
        try {
            $result = $this->_article->where('id', $id)->update([
                'title' => $request->title,
                'description' => $request->description,
                'content' => $request->content,
                'image' => $request->image,
                'status' => $request->status
            ]);
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Delete employees
     * @param string id
     *
     * @author huynhtuvinh87@gmail.com
     */
    public function delete($id)
    {
        try {
            $result = $this->_article->where('id', $id)->delete();
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }
}
