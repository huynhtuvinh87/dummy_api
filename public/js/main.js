$(document).on("submit", "form.form-action-delete", function() {});
$("#success-alert").hide();

$(document).on("click", ".buy", function() {
    id = $(this).attr("data-id");
    img = $("#product-" + id + " img").attr("src");
    title = $("#product-" + id + " img").attr("alt");
    url = $("#product-" + id + " .product-detail").attr("href");
    price = $("#product-" + id + " .price").text();
    var myElem = document.getElementById("userID");
    if (myElem === null) {
        location.href = "/login";
    }
    $.post(
        "/order/add",
        {
            _token: $('meta[name="csrf-token"]').attr("content"),
            product_name: title,
            product_price: price,
            product_img: img,
            product_url: url
        },
        function(data) {
            $("#success-alert")
                .fadeTo(2000, 1000)
                .slideUp(1000, function() {
                    $("#success-alert").slideUp(1000);
                });
        }
    );
});
