@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Features</div>

                <div class="card-body">
                    <ul>
                        <li>Fully secured quick prototyping.</li>
                        <li>Response format negotiation. (supporting JSON and XML)</li>
                        <li>Support for selectable output fields.</li>
                        <li>Full search support on all fields</li>
                        <li>Proper formatting of collection data and validation errors.</li>
                        <li>Support for the OPTIONS and HEAD verbs;</li>
                        <li>Support for <a href="//en.wikipedia.org/wiki/HATEOAS" target="_blank" rel="noopener">HATEOAS.</a></li>
                        <li>Supports <a href="http://en.wikipedia.org/wiki/Basic_access_authentication" rel="noopener" target="_blank">HTTP Basic Auth, </a>Query
                            parameter and HTTP Bearer Tokens
                        </li>
                        <li>Custom rate limiting feature.</li>
                        <li>Web interface for data management.</li>
                        <li>Usage history (basic troubleshooting for your requests)</li>
                    </ul>
                </div>
            </div>


            <div class="card" style="margin-top:20px">
                <div class="card-header">Authentication</div>

                <div class="card-body">
                    <p>There are different ways to send an access token:</p>
                    <ul>
                        <li class="gutter-sm-bottom">
                            <a href="http://en.wikipedia.org/wiki/Basic_access_authentication" target="_blank">HTTP Basic Auth</a>: the access token is sent as the
                            username.
                        </li>
                        <li class="gutter-sm-bottom">
                            Query parameter: the access token is sent as a query parameter in the API URL, e.g. <code>http://dummy.hudoshop.com/v1/api/articles?access-token=xxxxxxxx</code>.
                        </li>
                        <li class="gutter-sm-bottom">
                            <a href="http://oauth.net/2/" target="_blank">OAuth 2</a>: the access token is obtained by the consumer from an authorization server and
                            sent to the API server via <a href="http://tools.ietf.org/html/rfc6750">HTTP Bearer Tokens</a>, according to the OAuth2 protocol.
                        </li>
                        <li class="text-info">
                            <strong>NOTE: </strong> To prevent abuse all API calls requires a access-token, which can be obtained by a
                            <a class="btn btn-success btn-sm" href="/register">free registration.</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Resources</div>

                <div class="card-body">
                    <p>Example: <code>http://dummy.hudoshop.com/v1/api/articles?access-token=xxxxxxxx</code></p>
                    <h4>Categories</h4>
                    <p><code>Paramater: parent_id, title, description</code></p>
                    <ul>
                        <li><code> GET http://dummy.hudoshop.com/api/v1/categories</code></li>
                        <li><code> GET http://dummy.hudoshop.com/api/v1/categories/{id}</code></li>
                        <li><code> POST http://dummy.hudoshop.com/api/v1/categories</code></li>
                        <li><code> PUT, PATCH http://dummy.hudoshop.com/api/v1/categories/{id}</code></li>
                        <li><code> DELETE http://dummy.hudoshop.com/api/v1/categories/{id}</code></li>
                        <li><code> GET http://dummy.hudoshop.com/api/v1/category-parent/{id}</code></li>
                    </ul>
                    <h4>Products</h4>
                    <p><code>Paramater: category_id, title, content, image, price, price_sale, quantily, status</code></p>
                    <ul>
                        <li><code> GET http://dummy.hudoshop.com/api/v1/products</code></li>
                        <li><code> GET http://dummy.hudoshop.com/api/v1/products?page=1&limit=10</code></li>
                        <li><code> GET http://dummy.hudoshop.com/api/v1/products/{id}</code></li>
                        <li><code> POST http://dummy.hudoshop.com/api/v1/products</code></li>
                        <li><code> PUT, PATCH http://dummy.hudoshop.com/api/v1/products/{id}</code></li>
                        <li><code> DELETE http://dummy.hudoshop.com/api/v1/products/{id}</code></li>
                        <li><code> GET http://dummy.hudoshop.com/api/v1/products/category/{id}</code></li>
                    </ul>
                    {{-- <h4>Articles</h4>
                    <p><code>Paramater: category_id, title, content, status</code></p>
                    <ul>
                        <li><code> GET http://dummy.hudoshop.com/api/v1/articles</code></li>
                        <li><code> GET http://dummy.hudoshop.com/api/v1/articles/{id}</code></li>
                        <li><code> POST http://dummy.hudoshop.com/api/v1/articles</code></li>
                        <li><code> PUT, PATCH http://dummy.hudoshop.com/api/v1/articles/{id}</code></li>
                        <li><code> DELETE http://dummy.hudoshop.com/api/v1/articles/{id}</code></li>
                    </ul> --}}
                    <h4>Members</h4>
                    <p><code>Paramater: name, email, password, password_confirmation</code></p>
                    <ul>
                        <p><code>
                        'headers' => [
                        'Accept' => 'application/json',
                        'Authorization' => 'Bearer .$token
                        ]
                        </code></p>
                        <li><code> POST('name','email','password','password_confirmation') http://dummy.hudoshop.com/api/v1/register</code></li>
                        <li><code> POST('email','password') http://dummy.hudoshop.com/api/v1/login</code></li>
                        <li><code> POST http://dummy.hudoshop.com/api/v1/logout</code></li>
                        <li><code> POST http://dummy.hudoshop.com/api/v1/me</code></li>
                        <li><code> PUT('name','email','address','birthday','phone') http://dummy.hudoshop.com/api/v1/me</code></li>
                        <li><code> PATCH('password','password_confirmation') http://dummy.hudoshop.com/api/v1/me</code></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
