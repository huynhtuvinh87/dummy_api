@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <p>Access token: <small style="color:red">{{ Auth::user()->access_token }}</small></p>
                    <a href="/update-token" class="btn btn-success btn-sm">Update Token</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
