<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::prefix('v1')->group(function () {
    // Route articles
    Route::get('articles', 'Api\ArticleController@index');
    Route::get('articles/{id}', 'Api\ArticleController@show');
    Route::post('articles', 'Api\ArticleController@store');
    Route::put('articles/{id}', 'Api\ArticleController@update');
    Route::patch('articles/{id}', 'Api\ArticleController@update');
    Route::delete('articles/{id}', 'Api\ArticleController@destroy');

    // Route products
    Route::get('products', 'Api\ProductController@index');
    Route::get('products/{id}', 'Api\ProductController@show');
    Route::post('products', 'Api\ProductController@store');
    Route::put('products/{id}', 'Api\ProductController@update');
    Route::patch('products/{id}', 'Api\ProductController@update');
    Route::delete('products/{id}', 'Api\ProductController@destroy');
    Route::get('products/category/{category_id}', 'Api\ProductController@category');

    // Route categories
    Route::get('categories', 'Api\CategoryController@index');
    Route::get('categories/{id}', 'Api\CategoryController@show');
    Route::get('category-parent/{category_id}', 'Api\CategoryController@parents');
    Route::post('categories', 'Api\CategoryController@store');
    Route::put('categories/{id}', 'Api\CategoryController@update');
    Route::patch('categories/{id}', 'Api\CategoryController@update');
    Route::delete('categories/{id}', 'Api\CategoryController@destroy');


    // Route login, register
    Route::post('login', 'Api\UserController@login');
    Route::post('register', 'Api\UserController@register');
    // Reset password
    Route::post('reset-password', 'Api\ResetPasswordController@sendMail');
    Route::put('reset-password/{token}', 'Api\ResetPasswordController@reset');

    Route::group(['middleware' => 'auth:api'], function () {

        Route::post('me', 'Api\UserController@me');
        Route::post('me/upload', 'Api\UserController@upload');
        Route::post('me/upload-base64', 'Api\UserController@uploadBase64');
        Route::put('me', 'Api\UserController@update');
        Route::patch('me', 'Api\UserController@changePassword');
        Route::delete('me', 'Api\UserController@logout');
    });
});
